import { Component } from '@angular/core';

import { SessionPage } from '../session/session';
import { SpeakerPage } from '../speaker/speaker';
import { SchedulePage } from '../schedule/schedule';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = SchedulePage;
  tab2Root = SessionPage;
  tab3Root = SpeakerPage;

  constructor() {

  }
}
