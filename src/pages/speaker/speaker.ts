import { Component, OnInit } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { SpeakerProvider } from '../../providers/speaker/speaker.provider';

@Component({
  selector: 'page-speaker',
  templateUrl: 'speaker.html'
})
export class SpeakerPage implements OnInit {
  speakerData: any;

  constructor(private speakerProvider: SpeakerProvider,
              private loadingCtrl: LoadingController) {}

  ngOnInit() {
    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });
    loader.present();
    this.getSpeakerData(loader);
  }

  getSpeakerData(loader?) {
    this.speakerProvider.getData().subscribe((data) => {
      this.speakerData = data;
      if (loader) {
        loader.dismiss();
      }
    });
  }

  doRefresh() {
    let loader = this.loadingCtrl.create({
      content: "Updating..."
    });
    loader.present();
    this.getSpeakerData(loader);
  }

}
