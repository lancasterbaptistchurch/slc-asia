import { Component, OnInit } from '@angular/core';
import { SessionProvider } from '../../providers/session/session.provider';
import { LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-session',
  templateUrl: 'session.html'
})
export class SessionPage implements OnInit {
  sessionData: any;
  currentHeader: string = '';

  constructor(private sessionProvider: SessionProvider,
              private loadingCtrl: LoadingController) {}

  ngOnInit() {
    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });
    loader.present();
    this.getSessionData(loader);
  }

  isHeader(record, recordIndex, records) {
    if (this.currentHeader == '' || record.date != this.currentHeader) {
      this.currentHeader = record.date;
      // let realDate = moment(record.date, "dddd, MMMM D, YYYY - h:mma");
      return record.date;
    }
    else {
      return null;
    }
  }

  getSessionData(loader?) {
    this.sessionProvider.getData().subscribe((data) => {
      this.sessionData = data;
      if (loader) {
        loader.dismiss();
      }
    });
  }

  doRefresh() {
    let loader = this.loadingCtrl.create({
      content: "Updating..."
    });
    loader.present();
    this.getSessionData(loader);
  }
}
