import { Component, OnInit } from '@angular/core';
import { ScheduleProvider } from '../../providers/schedule/schedule.provider';
import { LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html'
})
export class SchedulePage implements OnInit {
  scheduleData: any;
  
  constructor(private scheduleService: ScheduleProvider,
              private loadingCtrl: LoadingController) {}

  ngOnInit() {
    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });
    loader.present();
    this.updateScheduleData(loader);
  }

  updateScheduleData(loader?) {
    this.scheduleService.getData().subscribe((data) => {
      this.scheduleData = data;
      if (loader) {
        loader.dismiss();
      }
    });
  }

  doRefresh() {
    let loader = this.loadingCtrl.create({
      content: "Updating..."
    });
    loader.present();
    this.updateScheduleData(loader);
  }

}
