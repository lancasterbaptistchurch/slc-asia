import { Injectable } from "@angular/core";
import { Http} from "@angular/http";
import { DataProvider } from "../data/data.provider";

@Injectable()
export class ScheduleProvider extends DataProvider {

  constructor(public http: Http) {
    super(http);
    this.url = "../../assets/schedule.json";
  }

}
