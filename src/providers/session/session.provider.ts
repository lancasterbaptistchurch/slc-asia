import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import 'rxjs/Rx';
import { DataProvider } from "../data/data.provider";

@Injectable()
export class SessionProvider extends DataProvider {

  constructor(public http:Http) {
    super(http);
    this.url = 'https://slconferenceasia.com/api/v1/sessions';
  }

}
