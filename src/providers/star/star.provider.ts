import { Injectable } from "@angular/core";
import 'rxjs/Rx';
import { Storage } from "@ionic/storage";

@Injectable()
export class StarProvider {
  private _starData: any[] = [];

  constructor(private storage: Storage) {
    this.getStarData();
  }

  get starData() {
    this.getStarData().then(val => {
      if (val != null) {
        this._starData = val;
      }
      else {
        this._starData = [];
      }
    });
    return this._starData.slice();
  }

  getStarData() {
    return this.storage.get("starred");
  }

  setStarData() {
    this.storage.set("starred", this._starData);
  }

  addStar(session) {
    this._starData.push(session);
    this.setStarData();
  }

  removeStar(sessionId) {
    let i = this._starData.indexOf(sessionId);
    this._starData.splice(i, 1);
    this.setStarData();
  }

  isInArray(sessionId) {
    let i = this._starData.indexOf(sessionId);
    if (i != -1) {
      return true;
    }
    else {
      return false;
    }
  }
}
