import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

@Injectable()
export class DataProvider {
  url: string;

  constructor(public http:Http) {}

  getData() {
    return this.http.get(this.url)
      .map((response) => response.json());
  }
}