import { Component, Input } from "@angular/core";
import * as moment from 'moment';

@Component({
  selector: 'session-list-item',
  templateUrl: 'session-list-item.component.html'
})
export class SessionListItem {
  @Input() session: any;
  @Input() showDate: boolean = false;

  constructor() {}

  formatDate(session) {
    let realDate = moment(session.date, "dddd, MMMM D, YYYY - h:mma");
    return realDate.format("ddd, MMM D - h:mma");
  }

}