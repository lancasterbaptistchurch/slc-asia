import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';

import { SessionPage } from '../pages/session/session';
import { SpeakerPage } from '../pages/speaker/speaker';
import { SchedulePage } from '../pages/schedule/schedule';
import { TabsPage } from '../pages/tabs/tabs';

import { ScheduleProvider } from '../providers/schedule/schedule.provider';
import { SessionProvider } from '../providers/session/session.provider';
import { SpeakerProvider } from '../providers/speaker/speaker.provider';
import { StarProvider } from '../providers/star/star.provider';
import { SessionListItem } from '../components/session-list-item/session-list-item.component';
import { DataProvider } from '../providers/data/data.provider';


@NgModule({
  declarations: [
    MyApp,
    SessionPage,
    SpeakerPage,
    SchedulePage,
    TabsPage,
    SessionListItem
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      tabsPlacement: 'bottom',
      platforms: {
        android: {
          tabsPlacement: 'top',
        }
      }
    }),
    HttpModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SessionPage,
    SpeakerPage,
    SchedulePage,
    TabsPage
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ScheduleProvider,
    SessionProvider,
    SpeakerProvider,
    StarProvider,
    DataProvider
  ]
})
export class AppModule {}
